$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 3000
    });
    $('#contacto').on('show.bs.modal',function(e){
        console.log('el modal se esta mostrando');
        $('#btn-puyo-contacto-id').removeClass('btn-outline-success');
        $('#btn-puyo-contacto-id').addClass('btn-primary');
        $('#btn-puyo-contacto-id').prop('disabled',true);
    });
    $('#contacto').on('hidden.bs.modal',function(e){
        console.log('el modal se oculto');
        $('#btn-puyo-contacto-id').prop('disabled',false);
        $('#btn-puyo-contacto-id').removeClass('btn-primary');
        $('#btn-puyo-contacto-id').addClass('btn-outline-success');
    });
    
});